#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <stdio.h>
#include "resource.h"

HINSTANCE hInst;

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch(uMsg) {
		case WM_INITDIALOG: {
		    CheckDlgButton (hwndDlg, CHK_SLOWUP, BST_CHECKED);
			}
		return TRUE;

		case WM_CLOSE: {
			EndDialog(hwndDlg, 0);
			}
		return TRUE;

		case WM_COMMAND: {
			switch(LOWORD(wParam)) {
				case BTN_LAUNCH: {
					system( "start https://blocktinu.com" );
					return (INT_PTR) TRUE;
					}
				case BTN_UPLOAD0: {
					system( "..\\bin\\blocktinu-upload.cmd ../samples/sample_empty.bthex" );
					return (INT_PTR) TRUE;
					}
				case BTN_UPLOAD1: {
					system( "..\\bin\\blocktinu-upload.cmd ../samples/sample_ledx5.bthex" );
					return (INT_PTR) TRUE;
					}
				case BTN_UPLOAD2: {
					system( "..\\bin\\blocktinu-upload.cmd ../samples/sample_edux3io.bthex" );
					return (INT_PTR) TRUE;
					}
				case BTN_UPLOAD3: {
					system( "..\\bin\\blocktinu-upload.cmd ../samples/sample_magx3io.bthex" );
					return (INT_PTR) TRUE;
					}
				case BTN_UPLOAD4: {
					system( "..\\bin\\blocktinu-upload.cmd ../samples/sample_kidsmicro.bthex" );
					return (INT_PTR) TRUE;
					}
				case BTN_UPLOAD5: {
					system( "..\\bin\\blocktinu-upload.cmd ../samples/sample_kidsmini.bthex" );
					return (INT_PTR) TRUE;
					}
				case BTN_CLOSE:
				case IDOK:
				case IDCANCEL: {
					EndDialog(hwndDlg, (INT_PTR) LOWORD(wParam));
					return (INT_PTR) TRUE;
					}
				case CHK_SLOWUP: {
					// system( "start https://blocktinu.com" );
					// Button_SetCheck(hwndDlg, BST_CHECKED);
					return (INT_PTR) TRUE;
					}
				}
			}
		return TRUE;
		}
	return FALSE;
	}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
	hInst=hInstance;
	InitCommonControls();
	return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
	}
