/**
 * Shield-LEDx5 - Testing scripts.
 * @author Neven Boyanov
 * This is part of the Tinusaur/Shield-LEDx5 project.
 * Also used in Blocktinu Tools as demo andtesting script.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2023 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/shield-ledx5
 */

// ============================================================================

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                   ATtiny85
//               +------------+
//               +        Vcc +---(+)-------
//       (RST)---+ PB5    Vcc +---(+)-------
// -------LED4---+ PB3    PB2 +---LED3------             (GREEN)
// -------LED5---+ PB4    PB1 +---LED2------ (GREEN) or  (YELLOW)
// --------(-)---+ GND    PB0 +---LED1------ (RED)       (RED)
//               +------------+                ^           ^
//               Tinusaur Board              LEDx2       LEDx3
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#define LED1_PORT PB0	// Define the LED I/O port
#define LED2_PORT PB1	// Define the LED I/O port
#define LED3_PORT PB2	// Define the LED I/O port
#define LED4_PORT PB3	// Define the LED I/O port
#define LED5_PORT PB4	// Define the LED I/O port

#define DEMO_DELAY 200

// ----------------------------------------------------------------------------

int main(void) {

	// ---- Initialization ----
	// Set the LED ports as output.
	DDRB |= (1 << LED1_PORT) | (1 << LED2_PORT) | (1 << LED3_PORT) | (1 << LED4_PORT) | (1 << LED5_PORT);

	// ---- Testing: Blink 5 LEDs ----
	for (;;) { // The infinite main loop
		for (uint8_t i = 1; i < (1 << 5); i <<= 1) {
			PORTB = (PORTB & 0b11100000) | i;
			_delay_ms(DEMO_DELAY);
		}
	}

	return 0; // Return the mandatory for the "main" function int value. It is "0" for success.
}

// ============================================================================
