
@ REM echo.
@ REM echo		########################
@ REM echo		##  BLOCKTINU UPLOAD  ##
@ REM echo		########################
@ REM echo.
@ echo blocktinu: #		##############
@ echo blocktinu: #		##  UPLOAD  ##
@ echo blocktinu: #		##############

@ REM cat %1
@ REM type %1

@ set PATH="%~dp0";%PATH%

@ avrdude -c usbasp -p t85 -B 3 -U flash:w:%1:a

@ del %1 2>nul
@ echo blocktinu: # The working .BTHEX file removed.

@ pause

