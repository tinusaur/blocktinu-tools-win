# This installs two files, blocktinu-app.exe and blocktinu.ico, creates a start menu shortcut, builds an uninstaller, and
# adds uninstall information to the registry for Add/Remove Programs

# To get started, put this script into a folder with the two files (blocktinu-app.exe, blocktinu.ico, and license.rtf -
# You'll have to create these yourself) and run makensis on it

# If you change the names "blocktinu-app.exe", "blocktinu.ico", or "license.rtf" you should do a search and replace - they
# show up in a few places.
# All the other settings can be tweaked by editing the !defines at the top of this script
!define APPNAME "Blocktinu"
!define COMPANYNAME "Tinusaur"
!define DESCRIPTION "The tools for the Blocktinu and the Tinusaur"
# These three must be integers
!define VERSIONMAJOR 1
!define VERSIONMINOR 3
!define VERSIONBUILD 2
!define VERSION_TEXT "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
# These will be displayed by the "Click here for support information" link in "Add/Remove Programs"
# It is possible to use "mailto:" links in here to open the email client
!define HELPURL "https://tinusaur.com" # "Support Information" link
!define UPDATEURL "https://tinusaur.com" # "Product Updates" link
!define ABOUTURL "https://tinusaur.com" # "Publisher" link
# This is the size (in kB) of all the files copied into "Program Files"
!define INSTALLSIZE 1000

RequestExecutionLevel admin ;Require admin rights on NT6+ (When UAC is turned on)

InstallDir "$PROGRAMFILES\${COMPANYNAME}\${APPNAME}"

# rtf or txt file - remember if it is txt, it must be in the DOS text format (\r\n)
LicenseData "LICENSE.txt"
# This will be in the installer/uninstaller's title bar
Name "${COMPANYNAME} - ${APPNAME}"
Icon "blocktinu.ico"
outFile "blocktinu-tools-${VERSION_TEXT}-install.exe"

!include LogicLib.nsh

# Just three pages - license agreement, install location, and installation
page license
page directory
Page instfiles

!macro VerifyUserIsAdmin
UserInfo::GetAccountType
pop $0
${If} $0 != "admin" ;Require admin rights on NT4+
        messageBox mb_iconstop "Administrator rights required!"
        setErrorLevel 740 ;ERROR_ELEVATION_REQUIRED
        quit
${EndIf}
!macroend

function .onInit
	setShellVarContext all
	!insertmacro VerifyUserIsAdmin
functionEnd

section "install"
	# Files for the install directory - to build the installer, these should be in the same directory as the install script (this file)
	setOutPath $INSTDIR
	# IMPORTANT: Files added here should be removed by the uninstaller (see section "uninstall")
	file "blocktinu.ico"
	# Add any other files for the install directory (license files, app data, etc) here

	# Install files in the "app" folder.
	setOutPath "$INSTDIR\app"
	file "app\blocktinu_app_win32.exe"
	file "app\libgcc_s_dw2-1.dll"
	file "app\libwinpthread-1.dll"
	
	# Install files in the "bin" folder.
	setOutPath "$INSTDIR\bin"
	# createDirectory "$INSTDIR\bin"	# Not necessary
	file "bin\blocktinu-upload.cmd"
	#
	file "bin\avrdude.exe"
	file "bin\avrdude.conf"
	# not needed: file "bin\libiconv-2.dll"
	file "bin\libusb0.dll"

	# Install files in the "samples" folder.
	setOutPath "$INSTDIR\samples"
	file "samples\sample_empty.bthex"
	file "samples\sample_ledx5.bthex"
	file "samples\sample_edux3io.bthex"
	file "samples\sample_magx3io.bthex"
	file "samples\sample_kidsmicro.bthex"
	file "samples\sample_kidsmini.bthex"
	
	# createDirectory "$INSTDIR\test"

	# Uninstaller - See function un.onInit and section "uninstall" for configuration
	writeUninstaller "$INSTDIR\uninstall.exe"

	# Start Menu
	createDirectory "$SMPROGRAMS\${COMPANYNAME}"
	createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk" "$INSTDIR\app\blocktinu_app_win32.exe" "" "$INSTDIR\blocktinu.ico"

	# Registry information for add/remove programs
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayName" "${COMPANYNAME} - ${APPNAME} - ${DESCRIPTION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayIcon" "$\"$INSTDIR\blocktinu.ico$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "Publisher" "${COMPANYNAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "HelpLink" "$\"${HELPURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLUpdateInfo" "$\"${UPDATEURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLInfoAbout" "$\"${ABOUTURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayVersion" "${VERSION_TEXT}"
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMajor" ${VERSIONMAJOR}
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMinor" ${VERSIONMINOR}
	# There is no option for modifying or repairing the install
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoRepair" 1
	# Set the INSTALLSIZE constant (!defined at the top of this script) so Add/Remove Programs can accurately report the size
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "EstimatedSize" ${INSTALLSIZE}
	
	# Add registry entries for BTHEX files association
	WriteRegStr HKCR ".bthex" "" "blocktinu_bthex"
	WriteRegStr HKCR "blocktinu_bthex" "" "Blocktinu BTHEX File - HEX code bundle"
	WriteRegStr HKCR "blocktinu_bthex" "Content Type" "application/blocktinu-bthex"
	WriteRegStr HKCR "blocktinu_bthex\DefaultIcon" "" "$INSTDIR\blocktinu.ico"
	WriteRegStr HKCR "blocktinu_bthex\shell\open\command" "" "$\"$INSTDIR\bin\blocktinu-upload.cmd$\" $\"%1$\""
	
sectionEnd

# Uninstaller

function un.onInit
	SetShellVarContext all

	#Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanently remove ${APPNAME} and all its components?" IDOK next
		Abort
	next:
	!insertmacro VerifyUserIsAdmin
functionEnd

section "uninstall"

	# Remove Start Menu launcher
	delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk"
	# Try to remove the Start Menu folder - this will only happen if it is empty
	rmDir "$SMPROGRAMS\${COMPANYNAME}"

	# Remove files from the root folder.
	delete $INSTDIR\blocktinu.ico

	# Remove files form "app" folder, remove the folder.
	delete "$INSTDIR\app\blocktinu_app_win32.exe"
	delete "$INSTDIR\app\libgcc_s_dw2-1.dll"
	delete "$INSTDIR\app\libwinpthread-1.dll"
	#
	# Remove files form "bin" folder, remove the folder.
	delete "$INSTDIR\bin\blocktinu-upload.cmd"
	#
	delete "$INSTDIR\bin\avrdude.exe"
	delete "$INSTDIR\bin\avrdude.conf"
	# not needed: delete "$INSTDIR\bin\libiconv-2.dll"
	delete "$INSTDIR\bin\libusb0.dll"
	#
	# Remove files form "samples" folder, remove the folder.
	delete "$INSTDIR\samples\sample_empty.bthex"
	delete "$INSTDIR\samples\sample_ledx5.bthex"
	delete "$INSTDIR\samples\sample_edux3io.bthex"
	delete "$INSTDIR\samples\sample_magx3io.bthex"
	delete "$INSTDIR\samples\sample_kidsmicro.bthex"
	delete "$INSTDIR\samples\sample_kidsmini.bthex"
	#
	# Remove folders
	rmDir "$INSTDIR\app"
	rmDir "$INSTDIR\bin"
	rmDir "$INSTDIR\samples"

	# Always delete uninstaller as the last action
	delete $INSTDIR\uninstall.exe

	# Try to remove the install directory - this will only happen if it is empty
	rmDir $INSTDIR

	# Remove uninstaller information from the registry
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}"
	
	# Remove registry entries for BTHEX files association
	DeleteRegKey HKCR ".bthex"
	DeleteRegKey HKCR "blocktinu_bthex"

sectionEnd
