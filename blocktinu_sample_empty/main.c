/**
 * Testing scripts.
 * @author Neven Boyanov
 * This is part of the Tinusaur/Blocktinu project.
 * 
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2023 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/shield-edux3io
 */

// ============================================================================

int main(void) {
	return 0; // Return the mandatory for the "main" function int value. It is "0" for success.
}

// ============================================================================
