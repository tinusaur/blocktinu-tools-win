/**
 * KidsMicro - Testing scripts.
 * @author Neven Boyanov
 * This is part of the Tinusaur/KidsMicro project.
 * Also used in Blocktinu Tools as demo andtesting script.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2023 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/kidsmicro
 */

// ============================================================================

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Board-Kids.Micro ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                 ATtiny85
//               +----------+
// ------(RST)---+ PB5  Vcc +---VCC(+)----
// -----BUTTON---+ PB3  PB2 +---PHOTORES--
// -----BUZZER---+ PB4  PB1 +---LED2(gre)-
// -----GND(-)---+ GND  PB0 +---LED1(red)-
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ----------------------------------------------------------------------------

// Ports definitions

#define KIDSMICRO_LED1 PB0	// Define the LED (red) I/O port
#define KIDSMICRO_LED2 PB1	// Define the LED (green) I/O port
#define KIDSMICRO_PHOTORES PB2	// Define the PHOTORES I/O port
#define KIDSMICRO_BUTTON PB3	// Define the BUTTON I/O port
#define KIDSMICRO_BUZZER PB4	// Define the BUZZER I/O port

// Define the ADMUX value to be used for the PHOTORES
// MUX[3:0] - 0000=ADC0(PB5); 0001=ADC1(PB2); 0010=ADC2(PB4); 0011=ADC3(PB3)
#define KIDSMICRO_PHOTORES_MUX (1 << MUX0)

// ----------------------------------------------------------------------------

#define BUTTON_DOWN() (!(PINB & (1 << KIDSMICRO_BUTTON)))

// ----------------------------------------------------------------------------

void buzzer_beep(uint16_t delay, uint16_t len) {
	for (; len > delay; len -= delay) {
		PORTB |= (1 << KIDSMICRO_BUZZER);
		_delay_loop_2(delay);
		PORTB &= ~(1 << KIDSMICRO_BUZZER);
		_delay_loop_2(delay);
	}
}

// ----------------------------------------------------------------------------

typedef union {
	uint16_t data;
	struct {
		uint8_t lo;
		uint8_t hi;
	};
} kidsmicro_photores;

int main(void) {
	// ---- Initialization ----

	// Set ports as output.
	DDRB |= (1 << KIDSMICRO_BUZZER);
	DDRB |= (1 << KIDSMICRO_LED1);
	DDRB |= (1 << KIDSMICRO_LED2);
	// Set ports as input.
	DDRB &= ~(1 << KIDSMICRO_BUTTON);	// Set the Button port as input
	PORTB |= (1 << KIDSMICRO_BUTTON);	// Set the Button port pull-up resistor

	// Init the ADC for the Photoresistor analog input
	ADMUX =
	    (0 << ADLAR) |	// Set left shift result - this SHOULD be 0
	    (KIDSMICRO_PHOTORES_MUX);	// Set ADCx input (on start conversions)
	ADCSRA =
	    (1 << ADEN) |	// Enable ADC
	    (1 << ADATE) |	// Enable auto trigger enable
	    (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);	// Prescaler: 111=7: 1/128th ... 1MHz / 128 = 7812Hz
	ADCSRB = 0;	// Set free running mode
	ADCSRA |= (1 << ADSC);	// Set start conversions
	_delay_ms(4); // Small delay may be necessary for correct first ADC reading.

	// ---- Main loop ----

	PORTB |= (1 << KIDSMICRO_LED1);		// Turn the LED on.
	PORTB &= ~(1 << KIDSMICRO_LED2);	// Turn the LED off.
	for (uint16_t d = 1000; d > 100; d -= 10) {
		buzzer_beep(d, 4000);	// Buzzer beep
		PORTB ^= (1 << KIDSMICRO_LED1); // Flip the LED on/off.
		PORTB ^= (1 << KIDSMICRO_LED2); // Flip the LED on/off.
		_delay_ms(20);
	}

	kidsmicro_photores photores;
	for (;;) { // Infinite main loop
		photores.lo = ADCL;
		photores.hi = ADCH;
		int16_t val, val_prev;
		val = photores.data;
		if (abs(val - val_prev) > 20 || BUTTON_DOWN()) {
			uint16_t buzzer_delay = (val >> 1) + 1;	// Divide the result, and "+1" to avoid zero value.
			if (BUTTON_DOWN()) { // Check the status of the button.
				buzzer_delay = (buzzer_delay >> 2) + 1;	// Divide the result, and "+1" to avoid zero value.
				PORTB ^= (1 << KIDSMICRO_LED2); // Flip the LED on/off.
			} else {
				PORTB &= ~(1 << KIDSMICRO_LED2); // Turn the LED off.
			}
			buzzer_beep(buzzer_delay, 8000);	// Buzzer beep
			PORTB ^= (1 << KIDSMICRO_LED1); // Flip the LED on/off.
		} else {
			PORTB &= ~(1 << KIDSMICRO_LED1); // Turn the LED off.
			PORTB &= ~(1 << KIDSMICRO_LED2); // Turn the LED off.
		}
		_delay_ms(100);
		val_prev = val;
	}

	return 0; // Return the mandatory for the "main" function int value. It is "0" for success.
}

// ============================================================================
