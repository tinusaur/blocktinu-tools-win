README

This is a demo and testing script for Blocktinu Tools.

To build the project, type:
	$ make

To upload the binary into the microcontroller, type:
	$ make program

Alternatively, to upload the binary, type:
	$ avrdude -c usbasp -p t85 -U flash:w:"main.hex":a

To clean up files left out from previous builds, type:
	$ make clean


NOTE: This is a copy of the tinusaur/shield_edux3io_demo1 project.

