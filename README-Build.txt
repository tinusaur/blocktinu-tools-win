Blocktinu Tools Win - The tools you need for Blocktinu to work under Microsoft Windows.


==== Files and Folders ====

Folders:
	avrdude_mingw32
	blocktinu_app_win
	blocktinu_app_win32
	blocktinu_sample_*
	blocktinu_upload
	install_nsis

---- Folder "avrdude_mingw32" ----

This folder contains the AVRDUDE executable and other necessary files.

The ".exe", ".conf" and ".dll" files from this folder should be copied/updated to the "/install_nsis/bin" folder before building the installation package with NSIS.


---- Folder "blocktinu_app_win" ----

This folder contains the "blocktinu_app_win32.exe" executable file along with other files that might be necessary such as DLL files.

This files are copied from the "blocktinu_app_win32/bin/Release" and other folders.

The files from this folder should be copied/updated to the "/install_nsis/app" folder before building the installation package with NSIS.


---- Folder "blocktinu_app_win32" ----

This is the project folder for the blocktinu_app_win32 application and contains its source code. Its product is a MS Windows executable file.

After building the "Release", the executable file from the "blocktinu_app_win32/bin/Release" folder should be copied/updated to the "blocktinu_app_win" folder to be bundled with other necessary files.


---- Folders "blocktinu_sample..." ----

These are the sample projects that produce HEX (.bthex) files.

Their .bthex files from the sub-folders should be copied/updated into the "install_nsis/samples" folder (before building the installation package with NSIS).


---- Folder "blocktinu_upload" ----

This is the script that uploads the HEX/BTHEX file to the microcontroller using the AVRDUDE.

The script file from this folder should be copied/updated to the "/install_nsis/bin" folder before building the installation package with NSIS.


==== Building the Installation Package ====

Open the "install_nsis.nsi" file, and ...

Check and if necessary update the version info in the file. Ref: VERSIONMAJOR, VERSIONMINOR, VERSIONBUILD.

Check the section "install" if there are new files to be added to the list of others that need to be removed.

Check the section "uninstall" if there are new files to be added to the list of others that need to be removed.


==== Creating a Release and Download Link at GitLab ====

Create a tag such as ver-1.2.3 -this will create a release as well.
Ref: https://gitlab.com/tinusaur/blocktinu-tools-win/-/tags

In the "Release notes" add a file attachment - the installation ZIP file. That will upload the ZIP file and will create a link suitable for sharing for downloading of the installation ZIP file.

